﻿using KR.Entity;
using KR.Repository;
using System.Collections.Generic;
using System.Web.Http;

namespace KR.Api.ApiControllers
{
    public class UserController : ApiController
    {
        private IRepository<User> repoUser;

        public UserController()
        {
            repoUser = new Repository<User>();
        }

        // GET api/user
        public IEnumerable<User> Get()
        {
            return repoUser.GetAll();
        }

        // GET api/user/5
        public User Get(int id)
        {
            return repoUser.Get(id);
        }

        // POST api/user
        public void Post([FromBody]User value)
        {
            value.CreatedDate = System.DateTime.Now;
            repoUser.Insert(value);
        }

        // PUT api/user/5
        public void Put(int id, [FromBody]string value)
        {
            //update
        }

        // DELETE api/user/5
        public void Delete(int id)
        {
           repoUser.Delete(repoUser.Get(id));
        }
    }
}