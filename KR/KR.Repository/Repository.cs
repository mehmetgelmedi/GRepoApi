﻿using KR.Data;
using KR.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace KR.Repository
{
    public class Repository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly KrContext krContext;
        private DbSet<T> entities;

        public Repository()
        {
            krContext = new KrContext();

            entities = krContext.Set<T>();
        }

        public Repository(KrContext krContext)
        {
            this.krContext = krContext;
            entities = krContext.Set<T>();
        }

        public T Get(long id)
        {
            return entities.FirstOrDefault(s => s.Id == id);
        }

        public IEnumerable<T> GetAll()
        {
            return entities.AsEnumerable();
        }

        public void Insert(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            entities.Add(entity);
            krContext.SaveChanges();
        }

        public void Update(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
        }

        public void Delete(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            entities.Remove(entity);
            krContext.SaveChanges();
        }
    }
}
