﻿using KR.Entity;
using System.Data.Entity;

namespace KR.Data
{
    public class KrContext  : DbContext
    {
        public KrContext() : base("name=KrDbContext")
        {

        }

        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            
        }
    }
}
